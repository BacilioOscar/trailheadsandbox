public class NewCaseListController {
    public List<Case> getNewCases(){
        return [Select CaseNumber from Case where Status='new'];
    }
}