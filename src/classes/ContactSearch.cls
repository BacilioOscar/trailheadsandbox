public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName,String postalCode){
        return [Select Name from Contact where lastName=:lastName and MailingPostalCode=:postalCode];
    }
}